#!/usr/bin/python
# -*- coding: utf-8 -*-

import urllib2
import json
import time

class Bitcurex(object):

    def __init__(self):
        self.refresh()

    def refresh(self):
        self.response_1 = urllib2.urlopen('https://pln.bitcurex.com/data/orderbook.json')
        self.price = json.loads(self.response_1.read())

    def get_asks(self):
        self.refresh()
        return [[float(offer[0]), float(offer[1])] for offer in self.price['asks']]

    def get_bids(self):
        self.refresh()
        return [[float(offer[0]), float(offer[1])] for offer in self.price['bids']]

class Bitstamp(object):

    def __init__(self, conversion_rate):
        self.refresh()
        self.conv_rate = conversion_rate
        #self.asks, self.bids = self.to_PLN()

    def refresh(self):
        self.response_2 = urllib2.urlopen('https://www.bitstamp.net/api/order_book/')
        self.price = json.loads(self.response_2.read())

    def get_asks(self):
        self.refresh()
        return [[float(offer[0]) * self.conv_rate, float(offer[0])] for offer in self.price['asks']]

    def get_bids(self):
        self.refresh()
        return [[float(offer[0]) * self.conv_rate, float(offer[0])] for offer in self.price['bids']]



class Arbitrage(object):
    """
    Main part of module
    Need PLN/USD exchange rate (float)
OPTIONAL amount of PLNs (float)
OPTIONAL minimum percentage profit to perform arbitrage transaction (float)
         values from 0 to 1.0
    """
    def __init__(self, ex_rate, PLN_wallet=100.0, min_profit=0):
        self.ex_rate = ex_rate
        self.min_profit = min_profit
        self.BTC_wallet = float(0)
        self.PLN_wallet = float(PLN_wallet)
        self.curex = Bitcurex()
        print "Curex Running"
        self.stamp = Bitstamp(self.ex_rate)
        print "Stamp Running"

    def check(self):

        #Get rid of "self" verbosity
        BTC_wallet = self.BTC_wallet
        PLN_wallet = self.PLN_wallet

        while True:

            #Fetch fresh data
            stampasks = self.stamp.get_asks()
            stampbids = self.stamp.get_bids()
            curexasks = self.curex.get_asks()
            curexbids = self.curex.get_bids()

            print "{:*^30}".format("TRANSACTIONS")
            print "{:^15}{:^15}".format("BTC", "PLN")
            print "{:^15.4f}{:^15.2f}".format(BTC_wallet, PLN_wallet)
            print "*" * 30

            while True:

                ask_stamp = stampasks.pop(0)
                bid_curex = curexbids.pop(0)

                print "{:^15}{:^15}".format("ASK STAMP", "BID CUREX")
                print "{:^15}{:^15}".format(ask_stamp[0], bid_curex[0])

                ask_curex = curexasks.pop(0)
                bid_stamp = stampbids.pop(0)

                print "{:^15}{:^15}".format("ASK CUREX", "BID STAMP")
                print "{:^15}{:^15}".format(ask_curex[0], bid_stamp[0])

                #Arbitrage opportunity
                # Sell on Bitcurex, Buy on Bitstamp
                if self.min_profit < ((bid_curex[0] - ask_stamp[0]) / ask_stamp[0]):

                    print "{:^30}".format("Buying on Bitstamp")

                    #Buy amount of BTC you can afford
                    if ask_stamp[1] > (PLN_wallet/ask_stamp[0]):
                        print "{0:^15}{1:^15}".format(PLN_wallet/ask_stamp[0], PLN_wallet)
                        BTC_wallet += PLN_wallet/ask_stamp[0]
                        PLN_wallet -= PLN_wallet

                    #Don't have enough PLN, buy for everything
                    else:
                        BTC_wallet += ask_stamp[1]
                        PLN_wallet -= ask_stamp[1] * ask_stamp[0] # BTCamount * BTCprice

                        print "{0:^15}{1:^15}".format(ask_stamp[1], ask_stamp[1] * ask_stamp[0])

                    print "{:^30}".format("Selling on Bitcurex")
                    #Sell as much as you can
                    if bid_curex[1] <= BTC_wallet:
                        BTC_wallet -= bid_curex[1]
                        PLN_wallet += bid_curex[1] * bid_curex[0] #BTCamount * BTCprice

                        print "{0:^15}{1:^15}".format(bid_curex[1], bid_curex[1] * bid_curex[0])

                    #Don't have enough BTC, sell everything
                    else:
                        print "{0:^15}{1:^15}".format(BTC_wallet, BTC_wallet * bid_curex[0])
                        PLN_wallet += BTC_wallet * bid_curex[0]
                        BTC_wallet -= BTC_wallet

                #Arbitrage opportunity
                # Sell on Bitstamp, buy on Bitcurex
                elif self.min_profit < ((bid_stamp[0] - ask_curex[0]) / ask_curex[0]):

                    print "{:^30}".format("Buying on Bitcurex")

                    #Buy max amount of BTC you can afford
                    if ask_curex[1] > (PLN_wallet/ask_curex[0]):
                        print "{0:^15}{1:^15}".format(PLN_wallet/ask_curex[0], PLN_wallet)
                        BTC_wallet += PLN_wallet/ask_curex[0]
                        PLN_wallet -= PLN_wallet
                    
                    #Don't have enough PLN, buy for everything
                    else:
                        BTC_wallet += ask_curex[1]
                        PLN_wallet -= ask_curex[1] * ask_curex[0] # BTCamount * BTCprice

                        print "{0:^15}{1:^15}".format(ask_curex[1], ask_curex[1] * ask_curex[0])

                    print "{:^30}".format("Selling on Bitstamp")
                    #Sell as much as you can
                    if bid_stamp[1] <= BTC_wallet:
                        BTC_wallet -= bid_stamp[1]
                        PLN_wallet += bid_stamp[1] * bid_stamp[0] #BTCamount * BTCprice

                        print "{0:^15}{1:^15}".format(bid_stamp[1], bid_stamp[1] * bid_stamp[0])

                    #Don't have enough BTC, sell everything
                    else:
                        print "{0:^15}{1:^15}".format(BTC_wallet, BTC_wallet * bid_stamp[0])
                        PLN_wallet += BTC_wallet * bid_stamp[0]
                        BTC_wallet -= BTC_wallet
                
                else:
                    #No Arbitrage opportunity
                    print "{:*^30}".format('END')
                    print "{:^15}{:^15}".format("BTC", "PLN")
                    print "{:^15.5f}{:^15.2f}".format(BTC_wallet, PLN_wallet)
                    print "*" * 30                    
                    break

            time.sleep(5)

if __name__ == "__main__":
    arbitrage = Arbitrage(3.1189, 100)
    arbitrage.check()


